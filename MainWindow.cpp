#include "gen/OrdersManagerBinding.nsmap"

#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QtDebug>

#define VARNAME_AUTH_EMAIL    "auth/email"
#define VARNAME_AUTH_PASSWD   "auth/passwd"
#define VARNAME_REFRESH_TIME  "orderlist/refresh"

static void init_ordersquery(ns1__OrdersQueryType* q)
{
  q->id = -127;
  q->idBusiness = -127;
  q->idDestination = -127;
  q->idDoer = -127;
  q->idOwner = -127;
}

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow),
  Settings("Shaa", "Orders Manager")
{
  ui->setupUi(this);

  ui_auth_clear_error();
  ui_reg_clear_error();
  ui_order_clear_error();

  connect(&RefreshTimer, SIGNAL(timeout()), this, SLOT(refresh()));

  init_ordersquery(&queryCurrent);

  switchViewPortPage(AuthPage);
  if (!Settings.value(VARNAME_AUTH_EMAIL).isNull())
  {
    ui->authSave->setChecked(true);
    ui->authEmail->setText(Settings.value(VARNAME_AUTH_EMAIL).toString());
    ui->authPassword->setText(Settings.value(VARNAME_AUTH_PASSWD).toString());
    on_buttonLogin_clicked();
  }

  if (Settings.value(VARNAME_REFRESH_TIME).isNull())
    Settings.setValue(VARNAME_REFRESH_TIME, (int)ui->cfgRefreshTime->value()*1000);
  else
    ui->cfgRefreshTime->setValue(Settings.value(VARNAME_REFRESH_TIME).toDouble()/1000);
}

MainWindow::~MainWindow()
{
  Client.destroy();
  delete ui;
}

void MainWindow::switchViewPortPage(MainWindow::ViewPortPage page)
{
  if (page == ListPage)
    RefreshTimer.start(Settings.value(VARNAME_REFRESH_TIME).toInt());
  else
    RefreshTimer.stop();
  switch(page)
  {
  case AuthPage:
    ui_auth_clear_error();
    isAuth = false;
    ui->authEmail->setText(Settings.value(VARNAME_AUTH_EMAIL).toString());
    ui->authPassword->setText(Settings.value(VARNAME_AUTH_PASSWD).toString());
    break;

  case RegPage:
    ui_reg_clear_data();
    ui_reg_clear_error();
    break;

  case MainPage:
    break;

  case ListPage:
    ui_list_clear_error();
    on_listMode_currentIndexChanged(ui->listMode->currentIndex());
    refresh();
    if (!isAuth)
      return;
    break;

  case OrderPage:
    ui_order_clear_error();
    break;

  case NewOrderPage:
    ui_neworder_clear_data();
    ui_neworder_clear_error();
    ui_neworder_update_data();
    break;

  case CfgPage:
    ui->cfgEmail->setText(Settings.value(VARNAME_AUTH_EMAIL).toString());
    ui->cfgPassword->setText(Settings.value(VARNAME_AUTH_PASSWD).toString());
    break;
  }
  ui->ViewPort->setCurrentIndex(page);
}

void MainWindow::refresh()
{
  ns1__clientAuthorizationInfoType auth;
  getAuthInfo(&auth);

  struct __ns1__FindOrdersResponse resp;
  if (Client.FindOrders(&queryCurrent, &auth, resp) != SOAP_OK)
  {
    clientFault("REFRESH");
    return;
  }
  switch (resp.code)
  {
  case ns1__responseCodeType__Success:
    break;

  case ns1__responseCodeType__EmailFailure:
    switchViewPortPage(AuthPage);
    ui_auth_error("Ошибочнй Email");
    return;

  case ns1__responseCodeType__PasswordFailure:
    switchViewPortPage(AuthPage);
    ui_auth_error("Не правельный пароль");
    return;

  default:
  case ns1__responseCodeType__Failure:
    qInfo() << "REFRESH: failure";
    return;
  }

  if (!resp.orders)
    return;

  for (unsigned int i = 0; i < resp.orders->data.size(); i++)
  {
    ns1__OrderType* order = resp.orders->data[i];

    QListWidgetItem* orderItem = 0;
    if (!OrdersList.contains(order->id))
    {
      orderItem = new QListWidgetItem(ui->OrdersList);
      orderItem->setData(Qt::UserRole, order->id);
    }
    else
    {
      for (int i = 0; i < ui->OrdersList->count(); i++)
      {
        QListWidgetItem* item = ui->OrdersList->item(i);
        if (item->data(Qt::UserRole).toInt() == order->id)
        {
          orderItem = item;
          break;
        }
      }
      Q_ASSERT(orderItem);
    }
    orderItem->setText(QString::fromStdString(order->Header));
    OrdersList[order->id] = *order;
  }
}

void MainWindow::on_buttonRegForm_clicked()
{
  switchViewPortPage(RegPage);
}

void MainWindow::clientFault(QString header)
{
  QString msg = QString::fromLocal8Bit(Client.soap_fault_string());
  if (!msg.isEmpty())
  {
    QString msgd = QString::fromLocal8Bit(Client.soap_fault_detail());
    if (!msgd.isEmpty())
      msg += ": " + msgd;
  }
  else
    msg = "uncknown error";
  qInfo() << header << msg;
}

void MainWindow::ui_auth_clear_error()
{
  ui->AuthError->setText(QString());
}

void MainWindow::ui_auth_error(QString text)
{
  ui->AuthError->setText(text);
}

void MainWindow::ui_reg_clear_error()
{
  ui->RegError->setText(QString());
}

void MainWindow::ui_reg_clear_data()
{
  ui->regEmail->setText(QString());
  ui->regPassword1->setText(QString());
  ui->regPassword2->setText(QString());
  ui->regFirstname->setText(QString());
  ui->regPatronymic->setText(QString());
  ui->regLastname->setText(QString());
  ui->regPhone->setText(QString());
}

void MainWindow::ui_reg_error(QString text)
{
  ui->RegError->setText(text);
}

void MainWindow::ui_list_clear_error()
{
  ui->ListError->setVisible(false);
  ui->ListError->setText(QString());
}

void MainWindow::ui_list_error(QString text)
{
  ui->ListError->setVisible(true);
  ui->ListError->setText(text);
}

void MainWindow::ui_order_clear_error()
{
  ui->OrderError->setVisible(false);
  ui->OrderError->setText(QString());
}

void MainWindow::ui_order_error(QString text)
{
  ui->OrderError->setVisible(true);
  ui->OrderError->setText(text);
}

void MainWindow::ui_neworder_clear_error()
{
  ui->NewOrderError->setVisible(false);
  ui->NewOrderError->setText(QString());
}

void MainWindow::ui_neworder_clear_data()
{
  ui->neworderHeader->setText("");
  ui->neworderPrice->setText("");
  ui->neworderDeadLine->setText("");
  ui->neworderDesc->setPlainText("");

  ui->neworderBussiness->clear();
  ui->neworderBussiness->addItem("", -1);
}

void MainWindow::buildBusinessNames(int idParent)
{
  ns1__idListType idList;
  if (Client.GetIdBusinessList(idParent, idList) != SOAP_OK)
  {
    clientFault("B_LIST");
    ui_neworder_error("Сбой соединения");
    return;
  }
  for (unsigned int i = 0; i < idList.id.size(); i++)
  {
    std::string str;
    if (Client.GetBusinessName(idList.id[i], str) != SOAP_OK)
    {
      clientFault("B_LIST");
      ui_neworder_error("Сбой соединения");
      return;
    }
    ui->neworderBussiness->addItem(QString::fromStdString(str), idList.id[i]);

    buildBusinessNames(idList.id[i]);
  }
}

void MainWindow::ui_neworder_update_data()
{
  buildBusinessNames();
}

void MainWindow::ui_neworder_error(QString text)
{
  ui->NewOrderError->setVisible(true);
  ui->NewOrderError->setText(text);
}

void MainWindow::getAuthInfo(ns1__clientAuthorizationInfoType* info)
{
  info->email = Settings.value(VARNAME_AUTH_EMAIL).toString().toStdString();
  info->passwd = Settings.value(VARNAME_AUTH_PASSWD).toString().toStdString();
}

void MainWindow::on_buttonLogin_clicked()
{
  ns1__clientAuthorizationInfoType info;
  info.email = ui->authEmail->text().toStdString();
  info.passwd = ui->authPassword->text().toStdString();

  ns1__responseCodeType response = ns1__responseCodeType__Failure;
  if (Client.Authorization(&info, response) != SOAP_OK)
  {
    clientFault("LOGIN");
    ui_auth_error("Сбой соединения");
    return;
  }

  std::string respStr;
  if (Client.GetResponseCodeString(response, respStr) != SOAP_OK)
  {
    clientFault("LOGIN: GetResponseCodeString");
    ui_auth_error("Сбой соединения");
  }

  switch(response)
  {
  case ns1__responseCodeType__Success:
    isAuth = true;
    if (Client.GetUserId(info.email, currentId) != SOAP_OK)
    {
      clientFault("LOGIN: get user id");
    }

    if (ui->authSave->isChecked())
    {
      Settings.setValue(VARNAME_AUTH_EMAIL, ui->authEmail->text());
      Settings.setValue(VARNAME_AUTH_PASSWD, ui->authPassword->text());
    }
    switchViewPortPage(MainPage);
    break;

  case ns1__responseCodeType__EmailFailure:
    ui_auth_error("Ошибочнй Email");
    break;

  case ns1__responseCodeType__PasswordFailure:
    ui_auth_error("Не правельный пароль");
    break;

  default:
  case ns1__responseCodeType__Failure:
    ui_auth_error("Неизвестная ошибка");
    break;
  }
}

void MainWindow::on_buttonRegCancel_clicked()
{
    switchViewPortPage(AuthPage);
}

void MainWindow::on_buttonMainExit_clicked()
{
  OrdersList.clear();
  ui->OrdersList->clear();
  switchViewPortPage(AuthPage);
}

void MainWindow::on_buttonRegistration_clicked()
{
  if (ui->regEmail->text().isEmpty())
  {
    ui_reg_error("Email не указан");
    return;
  }
  if (ui->regPassword1->text() != ui->regPassword2->text())
  {
    ui_reg_error("Пароли не совпадают");
    return;
  }
  if (ui->regFirstname->text().isEmpty() || ui->regPatronymic->text().isEmpty() || ui->regLastname->text().isEmpty())
  {
    ui_reg_error("ФИО -- обязательные поля");
    return;
  }

  ns1__clientFullInfoType info;
  ns1__clientAuthorizationInfoType auth;
  ns1__clientBaseInfoType base;
  info.authInfo = &auth;
  info.baseInfo = &base;

  info.authInfo->email = ui->regEmail->text().toStdString();
  if (!ui->regPassword1->text().isEmpty())
    info.authInfo->passwd = ui->regPassword1->text().toStdString();

  info.baseInfo->FirstName = ui->regFirstname->text().toStdString();
  info.baseInfo->Patronymic = ui->regPatronymic->text().toStdString();
  info.baseInfo->LastName = ui->regLastname->text().toStdString();
  info.baseInfo->phone = ui->regPhone->text().toStdString();

  ns1__responseCodeType response = ns1__responseCodeType__Failure;
  if (Client.Registration(&info, response) != SOAP_OK)
  {
    clientFault("REG");
    ui_reg_error("Сбой соединения");
    return;
  }

  std::string respStr;
  if (Client.GetResponseCodeString(response, respStr) != SOAP_OK)
  {
    clientFault("REG: GetResponseCodeString");
    ui_reg_error("Сбой соединения");
  }

  switch(response)
  {
  case ns1__responseCodeType__Success:
    switchViewPortPage(MainPage);
    break;

  case ns1__responseCodeType__EmailFailure:
    ui_reg_error("Такой Email уже зарегистрирован");
    break;

  case ns1__responseCodeType__FirstnameFailure:
  case ns1__responseCodeType__PatronymicFailure:
  case ns1__responseCodeType__LastnameFailure:
    ui_reg_error("ФИО -- обязательные поля");
    break;

  default:
  case ns1__responseCodeType__Failure:
    ui_reg_error("Неизвестная ошибка");
    break;
  }
}

void MainWindow::on_buttonConfig_clicked()
{
  switchViewPortPage(CfgPage);
}

void MainWindow::on_buttonCfgExit_clicked()
{
  switchViewPortPage(MainPage);
}

void MainWindow::on_cfgRefreshTime_editingFinished()
{
  Settings.setValue(VARNAME_REFRESH_TIME, ui->cfgRefreshTime->value()*1000);
  RefreshTimer.setInterval(Settings.value(VARNAME_REFRESH_TIME).toInt());
}

void MainWindow::on_cfgEmail_editingFinished()
{
  Settings.setValue(VARNAME_AUTH_EMAIL, ui->cfgEmail->text());
}

void MainWindow::on_cfgPassword_editingFinished()
{
  Settings.setValue(VARNAME_AUTH_PASSWD, ui->cfgPassword->text());
}

void MainWindow::on_OrdersList_itemClicked(QListWidgetItem *item)
{
  currentOrderId = item->data(Qt::UserRole).toInt();
  if (!OrdersList.contains(currentOrderId))
  {
    qInfo() << "ORDER: error";
    return;
  }

  switchViewPortPage(OrderPage);

  std::string businessName;
  if (Client.GetBusinessName(OrdersList[currentOrderId].idBusiness, businessName) != SOAP_OK)
    ui_order_error("Ошибка получения названия деятельности");
  if (businessName == "")
    businessName = "Неизвестно";

  ns1__clientBaseInfoType ownerInfo;
  if (Client.GetUserInfo(OrdersList[currentOrderId].idOwner, ownerInfo) != SOAP_OK)
    ui_order_error("Ошибка получения данных о заказчике");

  QString ownerName;
  if (!ownerInfo.FirstName.empty())
    ownerName = QString::fromStdString(ownerInfo.FirstName);
  if (!ownerInfo.Patronymic.empty())
    ownerName += ownerName.isEmpty()?(""):(" ") + QString::fromStdString(ownerInfo.Patronymic);
  if (!ownerInfo.LastName.empty())
    ownerName += ownerName.isEmpty()?(""):(" ") + QString::fromStdString(ownerInfo.LastName);
  if (ownerName.isEmpty())
    ownerName = "Неизвестен";

  QString phone = QString::fromStdString(ownerInfo.phone).simplified().split(" ").join("\n");

  ui->orderHeader->setText(QString::fromStdString(OrdersList[currentOrderId].Header));
  ui->orderBusiness->setText(QString::fromStdString(businessName));
  ui->orderOwner->setText(ownerName);
  ui->orderOwnerPhone->setText(phone);
  ui->orderPrice->setText(QString::fromStdString(OrdersList[currentOrderId].Price));
  ui->orderDeadline->setText(QString::fromStdString(OrdersList[currentOrderId].Deadline));
  ui->orderDescription->setText(QString::fromStdString(OrdersList[currentOrderId].Description));

  ui->buttonTakeOrder->setVisible(false);
  ui->buttonFinishOrder->setVisible(false);
  if (OrdersList[currentOrderId].idDoer == -1)
  {
    ui->buttonTakeOrder->setVisible(true);
  }
  else if ((OrdersList[currentOrderId].idDoer == currentId)&&(OrdersList[currentOrderId].isFinished == false))
  {
    ui->buttonFinishOrder->setVisible(true);
  }
}

void MainWindow::on_buttonOrderExit_clicked()
{
    switchViewPortPage(ListPage);
}

void MainWindow::on_buttonListMain_clicked()
{
    switchViewPortPage(MainPage);
}

void MainWindow::on_buttonList_clicked()
{
    switchViewPortPage(ListPage);
}

void MainWindow::on_listMode_currentIndexChanged(int index)
{
  switch (index)
  {
  case 0:
    queryCurrent.idDoer = -1;
    break;

  case 1:
    queryCurrent.idDoer = currentId;
    queryCurrent.isFinished = false;
    break;

  case 2:
    queryCurrent.idDoer = currentId;
    queryCurrent.isFinished = true;
    break;
  }
  ui->OrdersList->clear();
  OrdersList.clear();
  refresh();
}

void MainWindow::on_buttonNewOrder_clicked()
{
    switchViewPortPage(NewOrderPage);
}

void MainWindow::on_buttonNewOrderExit_clicked()
{
    switchViewPortPage(MainPage);
}

void MainWindow::on_buttonNewOrderSend_clicked()
{
  ui_neworder_clear_error();

  if (ui->neworderHeader->text().isEmpty())
  {
    ui_neworder_error("Заголовок не указан");
    return;
  }

  ns1__OrderType order;
  order.idOwner = currentId;
  order.idDestination = -1;
  order.idBusiness = ui->neworderBussiness->currentData().toInt();
  order.idDoer = -1;
  order.Header = ui->neworderHeader->text().toStdString();
  order.Price = ui->neworderPrice->text().toStdString();
  order.Deadline = ui->neworderDeadLine->text().toStdString();
  order.Description = ui->neworderDesc->toPlainText().toStdString();
  order.isFinished = false;

  ns1__clientAuthorizationInfoType auth;
  getAuthInfo(&auth);
  ns1__responseCodeType response = ns1__responseCodeType__Failure;
  if (Client.SendOrder(&order, &auth, response) != SOAP_OK)
  {
    clientFault("SEND");
    ui_neworder_error("Сбой соединения");
    return;
  }

  std::string respStr;
  if (Client.GetResponseCodeString(response, respStr) != SOAP_OK)
  {
    clientFault("SEND: GetResponseCodeString");
    ui_neworder_error("Сбой соединения");
  }

  switch(response)
  {
  case ns1__responseCodeType__Success:
    switchViewPortPage(MainPage);
    break;

  default:
  case ns1__responseCodeType__Failure:
    ui_neworder_error("Неизвестная ошибка");
    break;
  }
}

void MainWindow::on_buttonTakeOrder_clicked()
{
  ui_order_clear_error();

  ns1__clientAuthorizationInfoType auth;
  getAuthInfo(&auth);
  ns1__responseCodeType response = ns1__responseCodeType__Failure;
  if (Client.TakeOrder(currentOrderId, &auth, response) != SOAP_OK)
  {
    clientFault("TAKE");
    ui_order_error("Сбой соединения");
    return;
  }
  switch(response)
  {
  case ns1__responseCodeType__Success:
    switchViewPortPage(ListPage);
    break;

  default:
  case ns1__responseCodeType__Failure:
    ui_order_error("Неизвестная ошибка");
    break;
  }
}

void MainWindow::on_buttonFinishOrder_clicked()
{
  ui_order_clear_error();

  ns1__clientAuthorizationInfoType auth;
  getAuthInfo(&auth);
  ns1__responseCodeType response = ns1__responseCodeType__Failure;
  if (Client.FinishOrder(currentOrderId, &auth, response) != SOAP_OK)
  {
    clientFault("TAKE");
    ui_order_error("Сбой соединения");
    return;
  }
  switch(response)
  {
  case ns1__responseCodeType__Success:
    switchViewPortPage(ListPage);
    break;

  default:
  case ns1__responseCodeType__Failure:
    ui_reg_error("Неизвестная ошибка");
    break;
  }
}
