win32 {
    GSOAP_HEADERS += $$system(cd gsoap & dir /S /B /A:-D *.h)
    for(file, GSOAP_HEADERS) HEADERS += $$file

    GSOAP_SOURCES += $$system(cd gsoap & dir /S /B /A:-D *.cpp)
    for(file, GSOAP_SOURCES) SOURCES += $$file
}

unix {
    GSOAP_HEADERS += $$system(find $$PWD/gsoap -name \*.h)
    for(file, GSOAP_HEADERS) HEADERS += $$file

    GSOAP_SOURCES += $$system(find $$PWD/gsoap -name \*.c*)
    for(file, GSOAP_SOURCES) SOURCES += $$file
}

INCLUDEPATH += $$PWD/gsoap $$PWD/gsoap/gen
