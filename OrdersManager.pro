#-------------------------------------------------
#
# Project created by QtCreator 2015-08-31T01:49:03
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OrdersManager
TEMPLATE = app

SOURCES += main.cpp\
        MainWindow.cpp

HEADERS  += MainWindow.h

FORMS    += MainWindow.ui

include(etc/gsoap.pri)

CONFIG += mobility
MOBILITY = 

RESOURCES += \
    resources.qrc

