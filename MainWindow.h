#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSettings>
#include <QTimer>
#include <QListWidgetItem>

#include <gen/soapOrdersManagerBindingProxy.h>

namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

  enum ViewPortPage {MainPage, ListPage, OrderPage, NewOrderPage, CfgPage, AuthPage, RegPage};
  void switchViewPortPage(ViewPortPage page);

private slots:
  void refresh();
  //-----------------------------------
  void on_buttonRegForm_clicked();
  void on_buttonLogin_clicked();
  void on_buttonRegCancel_clicked();
  void on_buttonMainExit_clicked();
  void on_buttonRegistration_clicked();
  void on_buttonConfig_clicked();
  void on_cfgRefreshTime_editingFinished();
  void on_buttonCfgExit_clicked();
  void on_cfgEmail_editingFinished();
  void on_cfgPassword_editingFinished();
  void on_OrdersList_itemClicked(QListWidgetItem *item);
  void on_buttonOrderExit_clicked();
  void on_buttonListMain_clicked();
  void on_buttonList_clicked();
  void on_listMode_currentIndexChanged(int index);

  void on_buttonNewOrder_clicked();

  void on_buttonNewOrderExit_clicked();

  void on_buttonNewOrderSend_clicked();

  void on_buttonTakeOrder_clicked();

  void on_buttonFinishOrder_clicked();

private:
  Ui::MainWindow *ui;
  QSettings Settings;
  QTimer RefreshTimer;

  bool isAuth;
  int currentId, currentOrderId;

  OrdersManagerBindingProxy Client;
  QMap<int,ns1__OrderType> OrdersList;

  ns1__OrdersQueryType queryCurrent;

  void clientFault(QString header);

  void ui_auth_clear_error();
  void ui_reg_clear_error();
  void ui_list_clear_error();
  void ui_order_clear_error();
  void ui_neworder_clear_error();

  void ui_reg_clear_data();
  void ui_neworder_clear_data();
  void ui_neworder_update_data();

  void ui_auth_error(QString text);
  void ui_reg_error(QString text);
  void ui_list_error(QString text);
  void ui_order_error(QString text);
  void ui_neworder_error(QString text);

  void getAuthInfo(ns1__clientAuthorizationInfoType *info);

  void buildBusinessNames(int idParent = -1);
};

#endif // MAINWINDOW_H
